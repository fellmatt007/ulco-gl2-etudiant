
#include "Board.hpp"
#include "Report.hpp"

void testBoard(Board & b) {
    std::cout << b.getTitle() << std::endl;
    b.add("item 1");
    b.add("item 2");

    ReportFile reporter("test.txt");
    reporter.report(b);
}

int main() {

    Board b1;
    testBoard(b1);

    return 0;
}

