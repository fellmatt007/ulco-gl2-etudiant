#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

class ReportFile {
    private:
        std::ofstream _ofs;
    public:
        ReportFile(std::string filename): _ofs(filename) {}
        void report(const Itemable & itemable) 
        {
            for (const std::string & item : itemable.getItems())
                _ofs << item << std::endl;
            _ofs << std::endl;
        }
};

class ReportStdout {
    public:
        void report(const Itemable & itemable) {
            for (const std::string & item : itemable.getItems())
                std::cout << item << std::endl;
            std::cout << std::endl;
        }
};