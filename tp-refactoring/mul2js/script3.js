
function mul2(n) {
    return n*2;
}

function handle(span, id) {
    return function() {
        span.innerHTML = mul2(id.value);
    }
}

function handle2(span, id) {
    return function() {
        span.innerHTML = mul2(id.value);
    }
}